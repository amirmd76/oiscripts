#!/bin/bash
if [ $# -lt 2 ] || [ "$1" == "-h" ] || [ "$1" == "--help" ];then
echo "Syntax: timer.sh binary_file time_limit"

else

for i in `ls tests/*.in`
do
	tout=`\time -f "%U %M" sh -c "timeout $2 sh -c $1 < $i > /dev/null 2>/dev/null" 2>&1`
	if [ "$?" == "124" ]
	then
		echo -e "$i: TLE"
	else
		echo -e "$i - T&M: $tout"
	fi
done
fi

