#!/bin/bash
if [ $# -lt 3 ] || [ "$1" == "-h" ] || [ "$1" == "--help" ];then
echo "Syntax: stress.sh generator_binary_file code_binary_file time_limit"
else

test=1
while [ 1 -eq 1 ]
do
$1 $RANDOM $RANDOM $RANDOM $RANDOM > in 2> /dev/null
timeout $3 $2 < in > out 2> /dev/null
[ $? -ne 0 ] && break
echo Passed test "#$test"
test=`expr $test + 1`
done
echo Stress Found!

fi
