# IOI Scripts

Getting started:

```bash
make build
```

## Usage

### Stressing

Example: finding a stress testcase for file `code.out` using generator `gen.out` and time limit is 2 seconds.

```bash
./stress.sh ./gen.out ./code.out 2
```

You can manually change line #9 of `stress.sh` to use command line arguments in generator.

### Testing

Example: Testing `./code.out` against naive solution `./naive.out` using generator `./gen.out`.


```bash
./tester.sh ./gen.out ./naive.out ./code.out
```

You can manually change line #6 of `tester.sh` to use command line arguments in generator.


### Generating testcases

First of all, you need to write the tests script in a file, like `tests.txt`. Here's an example:


```
0
cat sample1.txt
cat sample2.txt
cat sample3.txt
1
./gen.out 1
./gen.out 10 mammad
2
./gen.out 10 mammad
python3 gen.py
cat strong.in
```

(Numbers before tests indicate number of subtask, here, there are three subtasks, 0, 1 and 2).

then you can generate tests running

```bash
rm -rf tests
mkdir tests
./sub < tests.txt
```

Note that for now `sub` uses `./code.out` as model solution for generating tests.

In this example, after running this command folder `tests` whill contain these files:

```
0-1.in
0-1.out
0-2.in
0-2.out
0-3.in
0-3.out
1-1.in
1-1.out
1-2.in
1-2.out
3-1.in
3-1.out
3-2.in
3-2.out
3-3.in
3-3.out
```
### Invocation

For finding time and memory it takes for `./code.out` to run on tests in folder `tests` with time limit 2s you can run:

```
./timer.sh ./code.out 2
```


### Zipping

For zipping tests (in folder `tests`) you can use this command:

```
./zip.sh
```

This will create a `tests.zip` file.

### Using

You can clone this repo and copy the folder for your problems. DO'NT FORGET TO DELETE `.git` FOLDER:

```
rm -rf .git
```


### Bugs/features
Reporintg bugs/requesting new feature: [email](mailto:amirmd76@gmail.com) or [telegram](https://t.me/amirmd76)


#### By AmirMohammad Dehghan
