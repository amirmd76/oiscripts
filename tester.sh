#!/bin/bash
if [ $# -lt 3 ] || [ "$1" == "-h" ] || [ "$1" == "--help" ];then
echo "Syntax: stress.sh generator_binary_file correct_binary_file code_binary_file"

for i in `seq 1 1000`;do
$1 $RANDOM $RANDOM $RANDOM $RANDOM > in 2> /dev/null;
$2 < in > ans 2> /dev/null;
$3 < in > out 2> /dev/null;
diff -bBq out ans;
[[ $? -ne 0 ]] && echo WA on test $i && break;
echo OK;done

